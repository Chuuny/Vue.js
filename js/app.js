new Vue({
    //  Change les délimiteurs afin d'integrer vue.js avec symfony
    /*delimiters: ['${', '}'],*/
    // l'id de l'élément
    el: '#app',
    // DATA = VARIABLE
    data: {
        // Le contenu de la variable
        message: 'Hello everyone',
        // Le contenu du lien
        link: 'http://anthony-bouillon.fr',
        // La variable vaut vrais ou faux, test : v-if et v-else
        success: false,
        // Tableau
        persons: ['Kenzi', 'Kenzi', 'Yuno', 'Bonnie'],
        // Utilisation de v-bind:style : la variable spécialisé dans le css, contient la couleur rouge
        colorChange: {
            color: 'red'
        },
        // Le contenu de la variable
        messageTrue: 'Tu m\'a coché',
        // Le contenu de la variable
        messageFalse: 'Tu ne m\'a pas coché',
        person: ''
    },
    // METHODS = FONCTION
    methods: {
        // Je créé la méthode close et quand elle est appelé : function()
        mouseOver: function() {
            // On change le contenu de la variable une fois que la fonction est active
            this.message = "Clique sur ce lien",
                //Il me met la variable success à false | Tout ce qui ce trouve dans data est accessible avec this
                this.link = 'http://portfolio.anthony-bouillon.fr',
                // On change la couleur du lien en rose
                this.colorChange.color = 'pink'
        },
        // La fonction est appelé aau clique
        addPerson: function() {
            // On ajoute dans le tableau ce que contient la variable person
            this.persons.push(this.person)
        }
    }
});
// On créé une nouvel instance
new Vue({
    el: '#time',
    data: {
        // La variable seconds est égale à 0
        seconds: 0
    },
    // est une hooks ( cycle de vie d'une instance )
    mounted: function() {
        this.$interval = setInterval(() => {
            console.log('Time')
                // Incrémente de 1
            this.seconds++
                // Arrivé à 11 secondes
                if (this.seconds > 10) {
                    // On détruit la fonction
                    clearInterval(this.$interval)
                }
                // La fonction s'execute toutes les secondes
        }, 1000);
    },
    // La fonction permet de détruire la fonction setInterval
    destroyed: function() {
        clearInterval(this.$interval)
    }
})